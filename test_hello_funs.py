"""Module providing a function printing python version."""
import unittest
import hello_funs

class HelloFunsTest(unittest.TestCase):
    """Class representing a test case for hello_funs.py."""

    def test_hello_world(self):
        """Function testing hello_funs.say_hello_world()."""
        self.assertEqual(hello_funs.say_hello_world(), "hello world ")
